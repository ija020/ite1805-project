<?php

define('DB_HOST', 'kark.uit.no');
define('DB_NAME', 'stud_v19_');
define('DB_USER', 'stud_v19_');
define('DB_PASSWORD', 'ditt passord');
define('DB_PORT', 3306);

if(defined('TESTING')) {
  define('DB_TABLE_PREFIX', 'test_');
} else {
  define('DB_TABLE_PREFIX', 'pugg_');
}

try {
  $db = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NAME,
    DB_USER, DB_PASSWORD);
  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch(PDOException $e) {
  die("Database connection failed: " . $e->getMessage());
}


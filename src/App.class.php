<?php
class App {
  private $db;

  public function __construct(PDO $db) {
    $this->db = $db;
  }

  public function exec(string $sql) : int {
    $sql = DbUtil::substitute($sql);
    return $this->db->exec($sql);
  }

  public function query(string $sql) : PDOStatement {
    $sql = DbUtil::substitute($sql);
    return $this->db->query($sql);
  }

  public function prepare(string $sql) : PDOStatement {
    $sql = DbUtil::substitute($sql);
    return $this->db->prepare($sql);
  }

  public function lastInsertId() : string {
    return $this->db->lastInsertId();
  }

  public function createUser(string $username, string $email, $password) : User {
    $sql = <<<SQL
      insert into #prefix#user
        (username, email, email_access_code, password_hash)
      values
        (:username, :email, :email_access_code, :password_hash);
SQL;
    $stmt = $this->prepare($sql);
    $stmt->bindParam(":username", $username);
    $stmt->bindParam(":email", $email);
    $stmt->bindValue(":email_access_code", base64_encode(random_bytes(30)));
    $stmt->bindValue(":password_hash", password_hash($password, PASSWORD_DEFAULT));
    $stmt->execute();
    return $this->getUser($username);
  }

  public function getUser(string $username) : User {
    $stmt = $this->prepare("select * from #prefix#user where username = :username;");
    $stmt->bindParam(":username", $username);
    $stmt->execute();
    $user = $stmt->fetchObject("User");
    if($user === FALSE) {
      throw new NotFoundException("No user by that name");
    }
    $user->setApp($this);
    return $user;
  }

  public function isUsernameTaken(string $username) : bool {
    try {
      $this->getUser($username);
    } catch(NotFoundException $e) {
      return false;
    }
    return true;
  }

  public function isEmailTaken(string $email) : bool {
    $stmt = $this->prepare("select email from #prefix#user where email = :email;");
    $stmt->bindParam(":email", $email);
    $stmt->execute();
    $row = $stmt->fetch(PDO::FETCH_ASSOC);
    if($row === FALSE) {
      return false;
    } else {
      return true;
    }
  }

  public function updateUser(User $user) {
    $sql = <<<SQL
      update #prefix#user set
        email=:email
      , email_confirmed=:email_confirmed
      , password_hash=:password_hash
      , is_admin=:is_admin
      , can_share_cards=:can_share_cards
      , can_comment=:can_comment
      , language_preference=:language_preference
      where username=:username;
SQL;
    $stmt = $this->prepare($sql);
    $stmt->bindValue(":username", $user->getUsername(), PDO::PARAM_STR);
    $stmt->bindValue(":email", $user->getEmail(), PDO::PARAM_STR);
    $stmt->bindValue(":email_confirmed", $user->isEmailConfirmed(), PDO::PARAM_BOOL);
    $stmt->bindValue(":password_hash", $user->getPasswordHash(), PDO::PARAM_STR);
    $stmt->bindValue(":is_admin", $user->isAdmin(), PDO::PARAM_BOOL);
    $stmt->bindValue(":can_share_cards", $user->canShareCards(), PDO::PARAM_BOOL);
    $stmt->bindValue(":can_comment", $user->canComment(), PDO::PARAM_BOOL);
    $stmt->bindValue(":language_preference", $user->getLanguagePreference(), PDO::PARAM_STR);
    $stmt->execute();
  }

  public function getNumberOfUsers() : int {
    $stmt = $this->query("select count(*) as number from #prefix#user;");
    $number = (int)$stmt->fetchColumn();
    return $number;
  }

  public function getUsers(int $offset = 0, int $limit = -1) : array {
    if($limit < 0) {
      $stmt = $this->prepare("select * from #prefix#user;");
    } else {
      $stmt = $this->prepare("select * from #prefix#user limit :limit offset :offset;");
      $stmt->bindParam(':limit', $limit, PDO::PARAM_INT);
      $stmt->bindParam(':offset', $offset, PDO::PARAM_INT);
    }

    $stmt->execute();
    $users = array();
    while($user = $stmt->fetchObject('User')) {
      $users[] = $user;
    }
    return $users;
  }

  // useful for testing
  public function deleteAllUsers() {
    $this->exec("delete from #prefix#user;");
  }

  // useful for testing
  public function deleteAllCommentables() {
    $this->exec("delete from #prefix#commentable;");
  }

  public function deleteAllCards() {
    $this->exec("delete from #prefix#card;");
  }

  public function createCard(User $author, string $front, string $back, string $info) : Card {
    $stmt = $this->prepare(<<<SQL
      insert into #prefix#card
        (owner, front, back, info)
      values
        (:owner, :front, :back, :info);
SQL
);
    $stmt->bindValue(":owner", $author->getUsername(), PDO::PARAM_STR);
    $stmt->bindParam(":front", $front, PDO::PARAM_STR);
    $stmt->bindParam(":back", $back, PDO::PARAM_STR);
    $stmt->bindParam(":info", $info, PDO::PARAM_STR);
    $stmt->execute();
    $cardId = (int)$this->lastInsertId();
    return $this->getCard($cardId);
  }

  public function getCard(int $cardId) : Card {
    $stmt = $this->prepare("select * from #prefix#card where card_id = :card_id;");
    $stmt->bindParam(":card_id", $cardId, PDO::PARAM_INT);
    $stmt->execute();
    $card = $stmt->fetchObject("Card");
    if($card === FALSE) {
      throw new NotFoundException("No card by that ID");
    }
    $card->setApp($this);
    return $card;
  }
}


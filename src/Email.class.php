<?php
class Email {
  // per header file
  // https://github.com/curl/curl/blob/master/include/curl/curl.h
  private const CURLE_PEER_FAILED_VALIDATION = 60;

  public static function sendConfirmationEmail(string $to, string $url) {
    // TODO: Check if we are running on kark.uit.no, try to use mail() if not
    try {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, "http://kark.uit.no/internett/php/mailer/mailer.php?address=" . urlencode($to) . "&url=" . urlencode($url));
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
      curl_setopt($ch, CURLOPT_MAXREDIRS, 5);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

      $ok = curl_exec($ch);
      if($ok === FALSE) {
        if(curl_errno($ch) === self::CURLE_PEER_FAILED_VALIDATION) {
          throw new Exception("SSL peer validation failed. Maybe configure curl.cainfo in php.ini?");
        }
        throw new Exception("curl_exec error code " . curl_errno($ch) . ": " . curl_error($ch));
      }

      $responseCode = curl_getinfo($ch, CURLINFO_RESPONSE_CODE);
      if($responseCode / 100 !== 2) {
        throw new Exception("Non-success HTTP code $responseCode returned when attempting to send mail through kark.uit.no's mail script.");
      }

    } finally {
      if(!empty($ch)) {
        curl_close($ch);
      }
    }   
  }
}


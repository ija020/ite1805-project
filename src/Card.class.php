<?php
class Card {
  private $app;
  private $card_id;
  private $owner;
  private $front;
  private $back;
  private $info;
  private $created;
  private $changed;
  private $shared;

  // this function is called after $stmt->fetchObject('Card') has
  // set member variables
  private function __construct() {
    $this->card_id = (int)$this->card_id;
    $this->created = DbUtil::parseDateTime($this->created);

    if(!empty($this->changed)) {
      $this->changed = DbUtil::parseDateTime($this->changed);
    }

    if(!empty($this->shared)) {
      $this->shared = DbUtil::parseDateTime($this->shared);
    }
  }

  public function setApp(App $app) : Card {
    $this->app = $app;
    return $this;
  }

  public function getId() : int {
    return $this->card_id;
  }

  public function getOwner() : User {
    return $this->app->getUser($this->owner);
  }

  public function getFront() : string {
    return $this->front;
  }

  public function getBack() : string {
    return $this->back;
  }

  public function getInfo() : string {
    return $this->info;
  }

  public function getCreated() : DateTime {
    return $this->created;
  }

  public function isChanged() : bool {
    return !empty($this->changed);
  }

  public function getChanged() : DateTime {
    return $this->changed;
  }

  public function isShared() : bool {
    return !empty($this->shared);
  }

  public function getShared() : DateTime {
    return $this->shared;
  }
  
}


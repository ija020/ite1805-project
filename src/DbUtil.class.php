<?php

use Phinx\Console\PhinxApplication;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Console\Input\StringInput;

class DbUtil {
  public static function substitute(string $sql) : string {
    return str_replace("#prefix#", DB_TABLE_PREFIX, $sql);
  }

  public static function dropEverything() {
    global $db;
    try {
      $db->exec('set foreign_key_checks=0;');

      $stmt = $db->query("show triggers like '" . DB_TABLE_PREFIX . "%';");
      $triggers = array();
      while($row = $stmt->fetch(PDO::FETCH_NUM)) {
        $triggers[] = $row[0];
      }
      foreach($triggers as $trigger) {
        $db->exec("drop trigger if exists " . $trigger . ";");
      }

      $stmt = $db->query("show tables like '" . DB_TABLE_PREFIX . "%';");
      $tables = array();
      while($row = $stmt->fetch(PDO::FETCH_NUM)) {
        $tables[] = $row[0];
      }
      foreach($tables as $table) {
        $db->exec("drop table if exists " . $table . ";");
      }

    } finally {
      if(!empty($stmt)) {
        $stmt->closeCursor();
      }
      $db->exec('set foreign_key_checks=1;');
    }
  }

  public static function phinxMigrate() {
    $phinx = new PhinxApplication();
    $phinx->setAutoExit(false);
    $statusCode = $phinx->run(new StringInput("migrate"), new NullOutput());
    if($statusCode != 0) {
      throw new Exception("phinx migration failed");
    }
  }

  public static function parseDateTime(string $dateTime) {
    return DateTime::createFromFormat("Y-m-d H:i:s", $dateTime);
  }
}


<?php
use Phinx\Migration\AbstractMigration;
use Phinx\Db\Adapter\AdapterInterface;
use Phinx\Db\Adapter\AbstractAdapter;
use Phinx\Db\Adapter\AdapterWrapper;

class DbMigration extends AbstractMigration {
  private function getTablePrefix() : string {
    $opt = $this->getAdapter()->getOption('table_prefix');
    if(gettype($opt) !== "string") {
      return "";
    }
    return $opt;
  }

  public function execute($sql) {
    $sql = DbUtil::substitute($sql);
    return parent::execute($sql);
  }

  public function query($sql) {
    $sql = DbUtil::substitute($sql);
    return parent::query($sql);
  }
}


<?php
class User {
  private $app;
  private $username;
  private $email;
  private $email_access_code;
  private $email_confirmed;
  private $password_hash;
  private $created;
  private $is_admin;
  private $can_comment;
  private $can_share_cards;
  private $language_preference;

  // called after $db->fetchObject
  private function __construct() {
    $this->created = DbUtil::parseDateTime($this->created);
  }

  public function getUsername() : string { return $this->username; }
  public function getEmail() : string { return $this->email; }
  public function getEmailAccessCode() : string { return $this->email_access_code; }
  public function getPasswordHash() : string { return $this->password_hash; }
  public function isAdmin() : bool { return $this->is_admin; }
  public function canComment() : bool { return $this->can_comment; }
  public function canShareCards() : bool { return $this->can_share_cards; }
  public function getLanguagePreference() : string { return $this->language_preference; }
  public function isEmailConfirmed() : bool { return $this->email_confirmed; }

  public function setApp(App $app) {
    $this->app = $app;
    return $this;
  }

  public function setEmail(string $email) : User {
    $this->email = $email;
    return $this;
  }

  public function setEmailConfirmed(bool $emailConfirmed) : User {
    $this->email_confirmed = $emailConfirmed;
    return $this;
  }

  public function setPassword(string $password) : User {
    $this->password_hash = password_hash($password, PASSWORD_DEFAULT);
    return $this;
  }

  public function setAdmin(bool $isAdmin) : User {
    $this->is_admin = $isAdmin;
    return $this;
  }

  public function setCanComment(bool $canComment) : User {
    $this->can_comment = $canComment;
    return $this;
  }

  public function setCanShareCards(bool $canShareCards) : User {
    $this->can_share_cards = $canShareCards;
    return $this;
  }

  public function isPassword(string $password) : bool {
    return password_verify($password, $this->password_hash);
  }

  public function isEmailAccessCode(string $accessCode) : bool {
    return $this->email_access_code == $accessCode;
  }

  public function getCreated() : DateTime {
    return $this->created;
  }

  public function commit() {
    $this->app->updateUser($this);
  }
}


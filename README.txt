Kopier auth_pdo_example.php til auth_pdo.php og endre tilkoblingsdetaljene i
auth_pdo.php. Start kommandolinjen og finn frem til denne mappen. Kjør noe
tilsvarende

    composer install

... for å hente pakker, og for å sette opp databasetabellene

    vendor\bin\phinx migrate

(bytt '\' med '/' hvis du kjører på Linux eller Mac)


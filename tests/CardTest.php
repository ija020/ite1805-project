<?php

require_once __DIR__ . "/autoload.php";

use PHPUnit\Framework\TestCase;

class CardTest extends TestCase {

  public static function setUpBeforeClass() : void {
    DbUtil::dropEverything();
    DbUtil::phinxMigrate();
  }

  private $app = NULL;
  private $bob = NULL;
  private $alice = NULL;

  public function setUp() : void {
    if($this->app === NULL) {
      global $db;
      $this->app = new App($db);
      $this->bob = $this->app->createUser("bob", "bob@example.com", "bob's password");
      $this->alice = $this->app->createUser("alice", "alice@example.com", "bob's password");
    }
    $this->app->deleteAllCards();
  }

  public function testCreateCard() {
    $card = $this->app->createCard($this->bob, "one in polish", "jeden", "");
    $cardId = $card->getId();
    $card = $this->app->getCard($cardId);
    $this->assertEquals("one in polish", $card->getFront());
    $this->assertEquals("jeden", $card->getBack());
  } 

}


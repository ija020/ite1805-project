<?php

require __DIR__ . "/autoload.php";

use PHPUnit\Framework\TestCase;
use Phinx\Console\PhinxApplication;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Input\StringInput;

class MigrationsTest extends TestCase {
  public function setUp() : void {
    DbUtil::dropEverything();
  }

  public function testMigrate() {
    $phinx = new PhinxApplication();
    $phinx->setAutoExit(false);

    $buffer = new BufferedOutput();
    $statusCode = $phinx->run(new StringInput("migrate --verbose"), $buffer);
    if($statusCode != 0) {
      echo $buffer->fetch();
    }
    $this->assertEquals(0, $statusCode, "phinx migration failed");

    $buffer = new BufferedOutput();
    $statusCode = $phinx->run(new StringInput("rollback -t 0 --verbose "), $buffer);
    if($statusCode != 0) {
      echo $buffer->fetch();
    }
    $this->assertEquals(0, $statusCode, "phinx rollback failed");

    $buffer = new BufferedOutput();
    $statusCode = $phinx->run(new StringInput("migrate --verbose"), $buffer);
    if($statusCode != 0) {
      echo $buffer->fetch();
    }
    $this->assertEquals(0, $statusCode, "phinx migration after rollback failed");
  }

}


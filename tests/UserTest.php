<?php

require_once __DIR__ . "/autoload.php";

use PHPUnit\Framework\TestCase;

class UserTest extends TestCase {
  private $app = NULL;

  const TEST_USERNAME = 'nils';
  const TEST_EMAIL = 'nils@example.com';
  const TEST_PASSWORD = 'a secret password';

  public static function setUpBeforeClass() : void {
    DbUtil::dropEverything();
    DbUtil::phinxMigrate();
  }

  public function setUp() : void {
    if($this->app === NULL) {
      global $db;
      $this->app = new App($db);
    }
    $this->app->deleteAllCommentables();
    $this->app->deleteAllUsers();
  }

  public function testCreateUser() {
    $user = $this->app->createUser(self::TEST_USERNAME, self::TEST_EMAIL, self::TEST_PASSWORD);
    $this->assertFalse(!$user, "Failed to create user");
    $this->assertEquals(self::TEST_USERNAME, $user->getUsername(), "Username of created user did not match parameteer passed to App::createUser");
    $user = $this->app->getUser(self::TEST_USERNAME);
    $this->assertFalse(!$user, "Failed to retrieve created user");
  }

  public function testPassword() {
    $user = $this->app->createUser(self::TEST_USERNAME, self::TEST_EMAIL, self::TEST_PASSWORD);
    $this->assertTrue($user->isPassword(self::TEST_PASSWORD), "Failed to confirm correct password");
    $this->assertFalse($user->isPassword("wrong passord"), "Confirmed wrong password");

    $newPassword = 'a new password';
    $user->setPassword($newPassword);
    $user->commit();

    $user = $this->app->getUser(self::TEST_USERNAME);
    $this->assertTrue($user->isPassword($newPassword), "Failed to confirm updated password");
  }

  public function testIsUsernameTaken() {
    $this->assertFalse($this->app->isUsernameTaken(self::TEST_USERNAME), "App::isUsernameTaken returned true before the username was taken");
    $this->app->createUser(self::TEST_USERNAME, self::TEST_EMAIL, self::TEST_PASSWORD);
    $this->assertTrue($this->app->isUsernameTaken(self::TEST_USERNAME), "App::isUsernameTaken returned false after the username was taken");
  }

  public function testIsEmailTaken() {
    $this->assertFalse($this->app->isEmailTaken(self::TEST_EMAIL), "App::isEmailTaken returned true before the email was taken");
    $this->app->createUser(self::TEST_USERNAME, self::TEST_EMAIL, self::TEST_PASSWORD);
    $this->assertTrue($this->app->isEmailTaken(self::TEST_EMAIL), "App::isEmailTaken returned false after the email was taken");
  }

  public function testGetNumberOfUsers() {
    $number = $this->app->getNumberOfUsers();
    $this->assertEquals(0, $number, "App::getNumberOfUsers returned non-zero before users where added");
    $this->app->createUser(self::TEST_USERNAME, self::TEST_EMAIL, self::TEST_PASSWORD);
    $number = $this->app->getNumberOfUsers();
    $this->assertEquals(1, $number, "App::getNumberOfUsers returned something other than 1 after a single user had been added");
  }

  public function testGetUsers() {
    $this->app->createUser('nils', 'nils@example.com', self::TEST_PASSWORD);
    $this->app->createUser('sara', 'sara@example.com', self::TEST_PASSWORD);
    $users = $this->app->getUsers();
    $this->assertEquals(2, count($users));
    $users = $this->app->getUsers(1, 2);
    $this->assertEquals('sara', $users[0]->getUsername());
  }
}


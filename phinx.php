<?php

require 'autoload.php';

global $db;

if(!defined('DB_TABLE_PREFIX')) {
  die("DB_TABLE_PREFIX must be defined in auth_pdo.php");
}

return [
  'migration_base_class' => 'DbMigration',
  'paths' => [
    'migrations' => '%%PHINX_CONFIG_DIR%%/db/migrations',
    'seeds' => '%%PHINX_CONFIG_DIR%%/db/seeds'
  ],
  'environments' => [
    'default_migration_table' => DB_TABLE_PREFIX . 'phinxlog',
    'default_database' => 'development',
    'development' => [
      'table_prefix' => DB_TABLE_PREFIX,
      'adapter' => 'mysql',
      'name' => DB_NAME,
      'user' => DB_USER,
      'pass' => DB_PASSWORD,
      'host' => DB_HOST,
      'port' => DB_PORT,
      'connection' => $db,
      'charset' => 'utf8'
    ]
  ]
];


<?php
require_once __DIR__ . '/auth_pdo.php';

require_once __DIR__ . '/vendor/autoload.php';

spl_autoload_register(function ($class) {
  $filepath = __DIR__ . '/src/' . $class . '.class.php';
  if(file_exists($filepath)) {
    require_once $filepath;
  }
}, FALSE);


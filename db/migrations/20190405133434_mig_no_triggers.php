<?php

class MigNoTriggers extends DbMigration
{
  function up() {
    $sql = <<<SQL
      create table #prefix#user (
        username varchar(80) primary key
      , email varchar(80) not null unique
      , email_confirmed boolean not null default false
      , email_access_code varchar(255) not null unique
      , password_hash varchar(255) not null
      , created datetime not null default (now())
      , is_admin boolean not null default false
      , can_share_cards boolean not null default true
      , can_comment boolean not null default true
      , language_preference varchar(10) not null default 'nb'
      ) engine=INNODB;
SQL;
    $this->execute($sql);

    $sql = <<<SQL
      create table #prefix#card (
        card_id integer primary key auto_increment
      , owner varchar(80) not null references #prefix#user(username)
      , front varchar(255) not null
      , back varchar(255) not null
      , info varchar(1024) not null
      , created datetime not null default (now())
      , changed datetime -- CAN BE NULL
      , shared datetime -- CAN BE NULL
      ) engine=INNODB;
SQL;
    $this->execute($sql);

    $sql = <<<SQL
      create table #prefix#card_tag (
        card_id integer not null references #prefix#card(card_id)
      , tag varchar(80) not null
      , seq integer not null -- determines the order of the tags of a card
      , primary key(card_id, tag)
      , unique(card_id, seq)
      ) engine=INNODB;
SQL;
    $this->execute($sql);
    
    $sql = <<<SQL
      create table #prefix#user_card_status (
        user varchar(80) not null references #prefix#user(username)
      , card_id integer not null references #prefix#card(card_id)
      , state varchar(15) not null
          check(state in ('new', 'again5min', 'again20min', 'review'))
      , easiness_factor double not null
      , last_interval integer not null
      , scheduled datetime not null
      , primary key(user, card_id)
      ) engine=INNODB;
SQL;
    $this->execute($sql);

    $sql = <<<SQL
      create table #prefix#site_setting (
        name varchar(80) primary key
      , type varchar(80) not null -- the type of values the setting can have
          check(type in ('boolean', 'string'))
      , value varchar(255) not null default (default_value)
      , default_value varchar(255) not null
      ) engine=INNODB;
SQL;
    $this->execute($sql);

    $sql = <<<SQL
      insert into #prefix#site_setting
        (name, type, default_value)
      values
        ('site-title', 'string', 'SITE TITLE')
      , ('enable-registration', 'boolean', 'true')
      , ('default-language', 'string', 'nb');
SQL;
    $this->execute($sql);

    $sql = <<<SQL
      create table #prefix#commentable (
        commentable_id varchar(255) primary key
      , locked boolean not null default false
      ) engine=INNODB;
SQL;
   $this->execute($sql);

    $sql = <<<SQL
      create table #prefix#thread (
        thread_id integer not null primary key
      , commentable_id varchar(255) not null references #prefix#commentable(commentable_id)
      , locked boolean not null default false
      , removed boolean not null default false
      ) engine=INNODB;
SQL;
    $this->execute($sql);

    $sql = <<<SQL
      create table #prefix#comment (
        thread_id integer not null references #prefix#thread(tid)
      , seq integer not null
      , author varchar(80) not null references #prefix#user(username)
      , posted datetime not null default (now())
      , content varchar(1024) not null
      , removed boolean not null default false
      , primary key(thread_id, seq)
      ) engine=INNODB;
SQL;
    $this->execute($sql);

    $sql = <<<SQL
      create table #prefix#metadeck (
        metadeck_id integer primary key auto_increment
      , name varchar(80) not null
      , owner varchar(80) not null references #prefix#user(username)
      , description varchar(255) not null
      , definition varchar(1024) not null -- defines the search criteria which will be interpreted by the application
      , shared datetime not null
      , unique(name, owner)
      ) engine=INNODB;
SQL;
    $this->execute($sql);

    $sql = <<<SQL
      create table #prefix#metadeck_subscription (
        metadeck_id integer not null references #prefix#metadeck(metadeck_id)
      , user varchar(80) not null references #prefix#user(username)
      , priority integer not null
      , primary key(metadeck_id, user)
      ) engine=INNODB;
SQL;
    $this->execute($sql);
  }

  function down() {
    $this->execute("drop table #prefix#metadeck_subscription;");
    $this->execute("drop table #prefix#metadeck;");
    $this->execute("drop table #prefix#comment;");
    $this->execute("drop table #prefix#thread;");
    $this->execute("drop table #prefix#commentable;");
    $this->execute("drop table #prefix#site_setting;");
    $this->execute("drop table #prefix#user_card_status;");
    $this->execute("drop table #prefix#card_tag;");
    $this->execute("drop table #prefix#card;");
    $this->execute("drop table #prefix#user;");
  }
}

